Ext.define('ExtJSMVC.view.Chart1', {
    extend: 'Ext.window.Window',
    alias:['widget.chart1'],

    height: 296,
    width: 480,
    layout: {
        type: 'fit'
    },
    title: 'My Window',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'chart',
		    store:'Store1',
		    animate:true,
		    highlight:true,
                    axes: [
                        {
                            type: 'Category',
                            fields: [
                                'category'
                            ],
                            position: 'left',
                            title: 'Category'
                        },
                        {
                            type: 'Numeric',
                            fields: [
                                'value'
                            ],
                            position: 'bottom',
                            title: 'Value',
			    minimum:0
                        }
                    ],
                    series: [
                        {
                            type: 'bar',
                            label: {
                                display: 'insideEnd',
                                field: 'category',
                                color: '#333',
                                'text-anchor': 'middle'
                            },
                            axis: 'bottom',
                            xField: 'category',
                            yField: [
                                'value'
                            ]
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }
});