Ext.define('ExtJSMVC.controller.View1', {

    extend:'Ext.app.Controller',

    views:[
	'View1'
    ],

    init:function(app){

	this.control({
	    'view1 > grid':{
		selectionchange:function(selectionModel, selected, eOpts){
		    selectionModel.getStore().fireEvent('selectionchange', selectionModel, {selected:selected} );
		},
		afterrender:function(component){
		    component.getStore().on('selectionchange', this.store_selectionchange);
		}
	    }
	    });
    },

    store_selectionchange:function(sender, args){

	Ext.each(Ext.ComponentQuery.query('view1 > grid'), function(view){

	    var selModel = view.getSelectionModel();

	    if(selModel != sender){
		selModel.suspendEvents();
		selModel.select(args.selected);
		selModel.resumeEvents();
	    }

	});
    }

});